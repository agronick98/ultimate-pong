# [Ultimate Pong](https://agronick98.gitlab.io/ultimate-pong/)

https://agronick98.gitlab.io/ultimate-pong/  
<br />

**Controls:**  

| Players 	    | Controls 	        |
|---------------|-------------------|
| 1  	        | Left/Right Arrow 	|
| 2  	        | , / 	            |
| 3  	        | A D 	            |
| 3  	        | C B 	            |